# IOS - Operating systems
# Project 1
[Assignment](assignment.pdf)  in Czech.

### Task details:
- Analyzer of records of person being infected with the coronavirus COVID-19
- Script filters the records of people with coronavirus infection 
- If a command is also given to the script, it executes the command over the filtered records 
- If no command is specified, the merge command is used by default
- If the script receives neither a filter nor a command, it prints records to stdout
- -s [WIDTH] for gender, age, daily, monthly, yearly, countries, districts and regions command lists data graphically in the form of histograms

### Run:
corona [-h] [FILTERS] [COMMAND] [LOG [LOG2 [...]]

## Evaluation 15/15

